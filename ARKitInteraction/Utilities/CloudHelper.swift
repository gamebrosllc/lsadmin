//
//  CloudHelper.swift
//  ARPersistence
//
//  Created by Gnerre on 10/17/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import CloudKit
import ARKit

class SavedLocation {
//    static var selectedLocation: SavedLocation?
//    static var shared = SavedLocation()
    
//    var mapData: Data!
    var record: CKRecord!
    
//    func convertRecord(record: CKRecord) -> SavedLocation {
//        let gp = SavedLocation()
//        gp.record = record
////        gp.mapData = record["mapData"]
//
//        return gp
//    }
}

class CloudHelper {
    static let shared = CloudHelper()
    
    private let recordID = CKRecordID.init(recordName: "map")
    static var currentLocation = CKRecord.init(recordType: "SavedLocation")

    
    let container = CKContainer.default()
    let publicDatabase = CKContainer.default().publicCloudDatabase
    let privateDatabase = CKContainer.default().privateCloudDatabase
    
    let locationManager = CLLocationManager()
    
    func initialize() {
        getMapFromCloud { message -> Void in
            print(message)
        }
    }
    
    func getMapFromCloud(onCompletion: @escaping (String) -> Void) {
        publicDatabase.fetch(withRecordID: recordID) { (record, error) in
            if error != nil {
                print(error?.localizedDescription)
                onCompletion(error!.localizedDescription)
                return
            }
            
            CloudHelper.currentLocation = record!
            onCompletion("Map loaded!")
        }

    }
    
    func saveWorldMap(record: CKRecord, onCompletion: @escaping (String) -> ()) {

        let saveOperation = CKModifyRecordsOperation(recordsToSave: [record], recordIDsToDelete: [])
        saveOperation.perRecordCompletionBlock = { (record: CKRecord?, error: Error?) -> Void in
            if let error = error {
                print(error.localizedDescription)
                onCompletion(error.localizedDescription)
                return
            }
            print("map saved!")
            onCompletion("Map saved!")
        }
        
        publicDatabase.add(saveOperation)
        
//            publicDatabase.save(mapRecord) {
//                (record, error) in
//                if let error = error {
//                    print(error.localizedDescription)
//                    return
//                }
//                print("map saved!")
//            }
    }
    
}
